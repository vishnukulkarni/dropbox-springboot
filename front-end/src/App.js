


import React from "react";
import './App.css';
import {Link, Route, Router} from "react-router-dom";
//import * as API from '../api/API';

//import {ResponsiveEmbed, Image} from 'react-bootstrap';
import createHistory from "history/createBrowserHistory";
import Activity from "./components/activity";
import Directory from "./components/directory";
import Home from "./components/home";
import Files from "./components/file";
import Welcome from "./components/welcome";
import Profile from "./components/profile";
import Groups from"./components/groups";
import Starredlist from "./components/starred_directory_list";
import newfolder from  "./components/newfolder";
const api = process.env.REACT_APP_CONTACTS_API_URL || 'http://localhost:8080';

var $ = require ('jquery');
const history = createHistory()
history.listen((location, action) => {

    // kafka activity api call


    // show / hide links
    var loggedInUserEmail = sessionStorage.getItem("loggedInUserEmail");
    if (loggedInUserEmail && loggedInUserEmail.length > 0) {
        $('[href="/login"]').hide()
        $('[href="/signup"]').hide()
        $('[href="/logout"]').show()
        $('[id="sidebar-wrapper"]').show()
        document.getElementsByClassName("main-content")[0].style.marginLeft = "200px";

    } else {
        $('[href="/login"]').show()
        $('[href="/signup"]').show()
        $('[href="/logout"]').hide()
        $('[id="sidebar-wrapper"]').hide()
        document.getElementsByClassName("main-content")[0].style.marginLeft = "0px";

    }

    // routes based on authentication
    if (location.pathname === '/logout') {
        sessionStorage.clear();
        history.push('/login')
    } else {
        //protected routes
        var protectedRoutes = ['/files', '/share', '/home','/directory','/groups'];
        if (protectedRoutes.indexOf(location.pathname) !== -1) {
            if (!loggedInUserEmail || loggedInUserEmail.length === 0) {
                history.push('/login')
            }
        }

    }

});
class Signup extends React.Component {

        constructor() {
            super();

            this.state = {
                password: "",
                email: "",
                phone: "",
                name: ""
            }
        }

        componentDidMount() {
            console.log(this)
        }

        handleSubmit(event) {
            event.preventDefault();

            fetch(`${api}/register`, {'mode': 'no-cors'}, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: this.state.email,
                        password: this.state.password,
                        phone: this.state.phone,
                        name: this.state.name
                    })
                }
            )
                .then(res => {
                    if (res.status === 200) {
                        alert("Sucessful sighup")
                        history.push('/login');
                    } else if (res.status === 400 || res.status === 500) {
                    }

                })
                .catch(error => {

                });

        }


        render() {
            return (
                <div className="container">

                        <div className="row">



                        <div className="col-md-6">
                            <img src="/components/avatars/home.jpg" alt="" />
                            </div>



                            <div className="col-md-5">
                            <form onSubmit={this.handleSubmit.bind(this)}>
                            <h3 className="form-signin-heading">Welcome! Please Sign Up</h3>
                            <hr className="colorgraph"/>
                            <br/>

                            <input value={this.state.email}
                                   onChange={(event) => {
                                       this.setState({
                                           email: event.target.value
                                       });
                                   }} type="text" className="form-control" name="Username" placeholder="Username"
                                   required=""
                                   autoFocus=""/>
                            <br/>
                            <input value={this.state.password}
                                   onChange={(event) => {
                                       this.setState({
                                           password: event.target.value
                                       });
                                   }} type="password" className="form-control" name="Password" placeholder="Password"
                                   required=""/>
                            <br/>

                            <input value={this.state.name}
                                   onChange={(event) => {
                                       this.setState({
                                           name: event.target.value
                                       });
                                   }} type="text" className="form-control" placeholder="email"
                                   required=""/>
                            <br/>

                            <input value={this.state.phone}
                                   onChange={(event) => {
                                       this.setState({
                                           phone: event.target.value
                                       });
                                   }} type="text" className="form-control" placeholder="Enter phone"
                                   required=""/>
                            <br/>

                            <button className="btn btn-lg btn-primary btn-block" type="submit" value="Register">Register
                            </button>
                        </form>

                            </div>
                            <div className="col-md-1">
                            </div>


                    </div>

                </div>
            )
        }
    }


class Login extends React.Component {

        constructor() {
            super();

            this.state = {
                password: "v",
                username: "v"
            }
        }

        componentDidMount() {
            sessionStorage.clear();
        }

        handleSubmit(event) {
            event.preventDefault();

            fetch(`${api}/login`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        username: this.state.username,
                        password: this.state.password
                    })
                }
            )
                .then((resp) => resp.json()) // Transform the data into json
                .then(res => {
                    sessionStorage.setItem("loggedInUserEmail", res.username)
                    history.push('/home')
                })
                .catch(error => {

                });

        }

        render() {
            return (
                <div className="container">

                            <div className="col-md-6">
                            <img src="/components/avatars/home.jpg" alt=""/>
                            </div>

                            <div className="col-md-5">
                            <form onSubmit={this.handleSubmit.bind(this)}>
                            <h3 className="form-signin-heading">Welcome Back! Please Sign In</h3>
                            <hr className="colorgraph"/>
                            <br/>

                            <input value={this.state.username}
                                   onChange={(event) => {
                                       this.setState({
                                           username: event.target.value
                                       });
                                   }} type="text" className="form-control" name="Username" placeholder="Username"
                                   required=""
                                   autoFocus=""/>
                            <br/>
                            <input value={this.state.password}
                                   onChange={(event) => {
                                       this.setState({
                                           password: event.target.value
                                       });
                                   }} type="password" className="form-control" name="Password" placeholder="Password"
                                   required=""/>

                            <br/>
                            <button className="btn btn-lg btn-primary btn-block" type="submit" value="Login">Login</button>
                        </form>
                            </div>

                            <div className="col-md-1">
                            </div>



                    </div>


            )
        }
    }

class App extends React.Component {
    constructor(){
        super();
        this.state = {
            loggedInUserEmail :sessionStorage.getItem("loggedInUserEmail")
        }
    }

    componentDidMount() {
        console.log(this)
    }

    render() {
        return (
            <Router history={history}>
                <div>
                    <nav className="navbar navbar-default">
                        <div className="container">
                            <div className="navbar-header">
                                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <a className="navbar-brand" href="/">


                                    <i className="glyphicon glyphicon-thumbs-up"></i> Drop Box
                                </a>

                            </div>

                            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul className="nav navbar-nav navbar-right">
                                    <li><Link to="/login">Login</Link></li>
                                    <li><Link to="/signup">Sign Up</Link></li>
                                    <li><Link to="/logout">Logout</Link></li>

                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div id="sidebar-wrapper" className="sidebar-toggle">
                        <div className="pl-18">
                            <img className="pl-13" src="/components/avatars/dbx2.jpg" alt=""/>
                            <div style={{'padding-left': '13px'}}>Vish</div>

                        </div>

                        <ul className="sidebar-nav">
                        <li><Link to="/profile">User Profile</Link></li>
                            <li><Link to="/home">Upload Files</Link></li>
                            <li><Link to="/files">Shared Files</Link></li>
                            <li><Link to="/directory">Directory</Link></li>
                            <li><Link to="/activity">Activity</Link></li>

                        </ul>
                    </div>


                    <div id="sidebar-wrapper" className="rightnavbar">


        <div id="custom-search-input">
        <div className="input-group col-md-12">
        <input type="text" className="  search-query form-control" placeholder="Search" />
        <span className="input-group-btn">
        <button className="btn btn-danger" type="button">
        <span className=" glyphicon glyphicon-search"></span>
        </button>
        </span>
        </div>
                        </div>
                        <ul className="sidebar-nav">
                        <li><Link to="/profile"></Link></li>
                            <li><Link to="/newfolder">New Folder</Link></li>
                            <li><Link to="/starred_directory_list">Starred Files</Link></li>
                            <li><Link to="/groups">Groups</Link></li>
                        </ul>
                    </div>




                    <div className="main-content">
                        <Route exact path="/" component={Welcome}/>
                        <Route path="/home" component={Home}/>
                        <Route path="/files" component={Files}/>
                        <Route path="/share" component={Share}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/signup" component={Signup}/>
                        <Route path="/activity" component={Activity}/>
                        <Route path="/directory" component={Directory}/>
                        <Route path="/profile" component={Profile}/>
                        <Route path="/groups" component={Groups}/>
                        <Route path="/starred_directory_list" component={Starredlist}/>
                        <Route path="/newfolder" component={newfolder}/>



                    </div>

                </div>
            </Router>
        )
    }
}



const Share = ({match}) => (
    <div>
        <h2>Share</h2>
        <ul>
            <li>
                <Link to={`${match.url}/rendering`}>
                    Rendering with React
                </Link>
            </li>
            <li>
                <Link to={`${match.url}/components`}>
                    Components
                </Link>
            </li>
            <li>
                <Link to={`${match.url}/props-v-state`}>
                    Props v. State
                </Link>
            </li>
        </ul>

        <Route path={`${match.url}/:topicId`} component={Topic}/>
        <Route exact path={match.url} render={() => (
            <h3>Please select a topic.</h3>
        )}/>
    </div>
);

const Topic = ({match}) => (
    <div>
        <h3>{match.params.topicId}</h3>
    </div>
);

export default App
/*
import React, {Component} from 'react';
import './App.css';

// import HomePage from "./components/HomePage";
// import NewHomePage from "./components/NewHomePage";

import {BrowserRouter} from 'react-router-dom';
import NewerHomePage from "./components/NewerHomePage";


// import HomePage from "./components/HomePage";

*/