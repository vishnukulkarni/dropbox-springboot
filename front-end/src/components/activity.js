


import React from "react";
import {Link, Route, Router} from "react-router-dom";
//import {ResponsiveEmbed, Image} from 'react-bootstrap';
import createHistory from "history/createBrowserHistory";
const api = process.env.REACT_APP_CONTACTS_API_URL || 'http://localhost:8080';
var $ = require ('jquery');
class Activity extends React.Component {

        constructor() {
            super();
            this.state = {
                activities: []
            }

        }
        componentDidMount() {
            var self = this;
            fetch(`${api}/activity`, {'mode': 'no-cors'}, {
                    method: 'POST'
            }
            )
                .then((resp) => resp.json()) // Transform the data into json
                .then(res => {
                    self.setState({activities: res});

                    console.log(res)
                })
                .catch(error => {

                });
        }

        render() {
            return (
                <div className="container">
                    <table className="table table-condensed">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Message</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.activities.map((item, index) => (
                            <tr key={item._id}>
                                <th scope="row">{index + 1}</th>
                                <td>{item.name}</td>
                                <td>{item.value.pathname}</td>
                            </tr>
                        ))}

                        </tbody>
                    </table>
                </div>
            )
        }
    }

    export default Activity