import React from "react";
import {Link, Route, Router} from "react-router-dom";
var $ = require ('jquery');


class Profile extends React.Component {
    constructor(){
        super();
        this.state = {
            loggedInUserEmail :sessionStorage.getItem("loggedInUserEmail")
        }
    }
            render() {
                return (


<div className="container">
	<div className="row">
		<div className="col-lg-3 col-sm-6">

            <div className="card hovercard">
                <div className="cardheader">

                </div>
                <div className="avatar">
                    <img alt="" src="components/avatars/profile.jpg"/>
                </div>
                <div className="info">
                    <div className="title">
                        <a target="_blank" href="http://scripteden.com/">{this.state.loggedInUserEmail}</a>
                    </div>
                    <div className="desc">Interests : Painting, Reading novels, Photograpgy</div>
                    <div className="desc">Work      : Intel Corp</div>
                    <div className="desc">Location  : San Jose,CA</div>
                </div>
                <div className="bottom">
                    <a className="btn btn-primary btn-twitter btn-sm" href="https://twitter.com/">
                        <i className="fa fa-twitter"></i>
                    </a>
                    <a className="btn btn-danger btn-sm" rel="publisher"
                       href="https://plus.google.com">
                        <i className="fa fa-google-plus"></i>
                    </a>
                    <a className="btn btn-primary btn-sm" rel="publisher"
                       href="https://www.facebook.com">
                        <i className="fa fa-facebook"></i>
                    </a>
                    <a className="btn btn-warning btn-sm" rel="publisher" href="https://plus.google.com">
                        <i className="fa fa-behance"></i>
                    </a>
                </div>
            </div>

        </div>

	</div>
</div>

                )
            }
        }

export default Profile