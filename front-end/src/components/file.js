import React from "react";
import {Link, Route, Router} from "react-router-dom";
//import {ResponsiveEmbed, Image} from 'react-bootstrap';
import createHistory from "history/createBrowserHistory";
var $ = require ('jquery');
//var loggedInUserEmail = 'v';
const api = process.env.REACT_APP_CONTACTS_API_URL || 'http://localhost:8080';

class Files extends React.Component {
    constructor() {
        super();
        this.state = {
            files: [],
            sharedfiles: [],
            selectedShareFile: null,
            emailId: null
        }

    }

    share(event) {
        var payload = {
            emailId: this.state.emailId,
            fileId: this.state.selectedShareFile,
            loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
        }

        console.log(payload)
        var self = this;
        fetch(`${api}/share_file`, {'mode': 'no-cors'}, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            }
        )
            .then(res => {

                console.log(res)
            })
            .catch(error => {

            });

    }

    componentDidMount() {
        var self = this;
        fetch(`${api}/files`, {'mode': 'no-cors'}, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                })

            }
        )
            .then((resp) => resp.json()) // Transform the data into json
            .then(res => {
                self.setState({files: res});

                console.log(res)
            })
            .catch(error => {

            });

        fetch(`${api}/shared_files_by_others`, {'mode': 'no-cors'}, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                })
            }
        )
            .then((resp) => resp.json()) // Transform the data into json
            .then(res => {
                self.setState({sharedfiles: res});

                console.log(res)
            })
            .catch(error => {

            });


    }


    render() {
        return (
            <div>

            <h2><small>Files/Folders shared by others</small></h2>
                <table className="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>File name</th>
                        <th>Mark Starred</th>
                        <th>Download</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.sharedfiles.map((item, index) => (
                        <tr key={item._id}>
                            <th scope="row">{index + 1}</th>
                            <td><i className="glyphicon glyphicon-file" style={{'fontSize': '18px', color:'#1e90ff'}}></i>{item.filename.split('_')[1]}</td>
                            <td>
                                <i className="glyphicon glyphicon-star"></i>
                            </td>
                            <td>
                                <a href={'/upload/' + item.filename} target="_blank"><i
                                    className="glyphicon glyphicon-download" style={{'fontSize': '28px', color:'#008000'}}></i> </a>
                            </td>
                            <td>
                                <button className="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target=".bs-example-modal-sm">Share
                                </button>
                            </td>
                        </tr>
                    ))}

                    </tbody>
                </table>


                <h2><small>My files and folders</small></h2>
                <table className="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>File name</th>
                        <th>Mark Starred</th>
                        <th>Download</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.files.map((item, index) => (
                        <tr key={item._id}>
                            <th scope="row">{index + 1}</th>
                            <td><i className="glyphicon glyphicon-file" style={{'fontSize': '18px', color:'#d6a10e'}} ></i>{item.filename.split('_')[1]}</td>
                            <td>
                                <i className="glyphicon glyphicon-star" style={{'fontSize': '18px'}}></i>
                            </td>
                            <td>
                                <a href={'/upload/' + item.filename} target="_blank"><i
                                    className="glyphicon glyphicon-download" style={{'fontSize': '28px', color:'#008000'}}></i> </a>
                            </td>
                            <td>
                                <button className="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target=".bs-example-modal-sm"
                                        onClick={(event) => {
                                            this.setState({
                                                selectedShareFile: item._id
                                            });
                                        }}
                                >Share
                                </button>
                            </td>
                        </tr>
                    ))}

                    </tbody>
                </table>
                <hr/>

               TBD

                <div className="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div className="modal-dialog modal-sm" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Share file with</h4>
                            </div>
                            <div className="modal-body">
                                <div className="input-group">
                                    <span className="input-group-addon" id="basic-addon3">Enter email id</span>
                                    <input type="text" className="form-control" id="basic-url"
                                           aria-describedby="basic-addon3"
                                           onChange={(event) => {
                                               this.setState({
                                                   emailId: event.target.value
                                               });
                                           }}
                                    ></input>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary" onClick={this.share.bind(this)}>Share
                                    Now
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Files