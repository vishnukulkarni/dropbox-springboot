import React from "react";
import {Link, Route, Router} from "react-router-dom";
//import {ResponsiveEmbed, Image} from 'react-bootstrap';
import createHistory from "history/createBrowserHistory";
var $ = require ('jquery');
const api = process.env.REACT_APP_CONTACTS_API_URL || 'http://localhost:8080';

class Groups extends React.Component {

    constructor() {
        super();
        this.state = {
            files: [],
            sharedfiles: [],
            selectedShareFile: null,
            emailId: null
        }

    }

    share(event) {
        var payload = {
            emailId: this.state.emailId,
            fileId: this.state.selectedShareFile,
            loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
        }

        console.log(payload)
        var self = this;
        fetch(`${api}/share_file`, {'mode': 'no-cors'}, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            }
        )
            .then(res => {

                console.log(res)
            })
            .catch(error => {

            });

    }
    componentDidMount() {
        var self = this;
        fetch(`${api}/files`, {'mode': 'no-cors'}, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                })

            }
        )
            .then((resp) => resp.json()) // Transform the data into json
            .then(res => {
                self.setState({files: res});

                console.log(res)
            })
            .catch(error => {

            });

        fetch(`${api}/shared_files_by_others`, {'mode': 'no-cors'}, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loggedInUserEmail: sessionStorage.getItem('loggedInUserEmail')
                })
            }
        )
            .then((resp) => resp.json()) // Transform the data into json
            .then(res => {
                self.setState({sharedfiles: res});

                console.log(res)
            })
            .catch(error => {

            });


    }
    render() {
        return (
            <div>

            <h2><small>Files/Folders shared by others</small></h2>
                <table className="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Group Name</th>
                        <th>Mark Starred</th>
                        <th>Download</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.sharedfiles.map((item, index) => (
                        <tr key={item._id}>
                            <th scope="row">{index + 1}</th>
                            <td><i className="glyphicon glyphicon-file" style={{'fontSize': '18px', color:'#1e90ff'}}></i></td>
                            <td>
                                <i className="glyphicon glyphicon-star"></i>
                            </td>
                            <td>
                                <a href={'/upload/' + item.filename} target="_blank"><i
                                    className="glyphicon glyphicon-download" style={{'fontSize': '28px', color:'#008000'}}></i> </a>
                            </td>
                            <td>
                                <button className="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target=".bs-example-modal-sm">Share
                                </button>
                            </td>
                        </tr>
                    ))}

                    </tbody>
                </table>

</div>
 )
}
}

export default Groups