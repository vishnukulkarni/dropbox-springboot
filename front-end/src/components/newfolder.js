import React from "react";
import {Link, Route, Router} from "react-router-dom";
//import {ResponsiveEmbed, Image} from 'react-bootstrap';
import createHistory from "history/createBrowserHistory";
var $ = require ('jquery');
const api = process.env.REACT_APP_CONTACTS_API_URL || 'http://localhost:8080';

class newfolder extends React.Component {

        componentDidMount() {
            console.log(this)
        }

        handleSubmit(event) {

            event.preventDefault();
            var self = this;
            if (this.refs.sampleFile !== '') {
                const payload = new FormData();

                payload.append('sampleFile', this.refs.sampleFile.files[0]);
                payload.append('loggedInUserEmail', sessionStorage.getItem("loggedInUserEmail"))

                fetch(`${api}/upload`, {'mode': 'no-cors'}, {
                        method: 'POST',
                        headers: {
                            // 'Content-Type': 'multipart/form-data'
                        },
                        body: payload
                    }
                )
                    .then(res => {
                        if (res.status == 200) {
                            alert("FOLDER uploaded")
                        } else if (res.status == 400 || res.status == 500) {
                            alert("FOLDER upload failed")
                        }

                        $("#sampleFile").val('');


                    })
                    .catch(error => {

                    });

                return;
            }
        }

        render() {
            return (
                <div className="container">
                    <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="row">


                    <div className="col-md-3">
                            </div>

                            <div className="col-md-5">


                                <input className="form-control glyphicon glyphicon-folder-open " ref="sampleFile" type="file" name="sampleFile" id="sampleFile"  style={{'fontSize': '24px', color:'#d6a10e'}}/>

                                <input className="btn btn-primary" type="submit" value="Upload Folder"/>

                            </div>
                            <div className="col-md-4">

                            </div>
                        </div>
                    </form>
                </div>
            )
        }
    }

    export default newfolder