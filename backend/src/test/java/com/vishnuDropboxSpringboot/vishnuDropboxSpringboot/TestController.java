package com.vishnuDropboxSpringboot.vishnuDropboxSpringboot;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
public class TestController extends vishnuDropboxSpringbootApplicationTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void uploadApi() throws Exception {
		mockMvc.perform(post("/upload")).andExpect(status().isNotFound());

	}
	
	@Test
	public void login() throws Exception {
		mockMvc.perform(post("/login")).andExpect(status().isNotFound());

	}
	
	@Test
	public void welcomeApi() throws Exception {
		mockMvc.perform(post("/welcome")).andExpect(status().isNotFound());

	}
	
	@Test
	public void share() throws Exception {
		mockMvc.perform(post("/share")).andExpect(status().isNotFound());

	}
	
	@Test
	public void directoryApi() throws Exception {
		mockMvc.perform(post("/list")).andExpect(status().isNotFound());

	}
	
	@Test
	public void listfiles() throws Exception {
		mockMvc.perform(post("/Directory")).andExpect(status().isNotFound());

	}
	
	@Test
	public void folder() throws Exception {
		mockMvc.perform(post("/folders")).andExpect(status().isNotFound());

	}
	
	@Test
	public void activityApi() throws Exception {
		mockMvc.perform(post("/user/signup")).andExpect(status().isNotFound());

	}
	
	@Test
	public void activitylog() throws Exception {
		mockMvc.perform(post("/activity")).andExpect(status().isNotFound());

	}
	
	@Test
	public void registerApi() throws Exception {
		mockMvc.perform(post("/registerApi")).andExpect(status().isNotFound());

	}

}