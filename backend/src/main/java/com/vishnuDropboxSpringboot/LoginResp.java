package com.vishnuDropboxSpringboot;


public class LoginResp {
    Resp Dbxresponse;
    Usr Dbxusers;

    public Resp getResponse() {
        return Dbxresponse;
    }

    public void setResponse(Resp Dbxresponse) {
        this.Dbxresponse = Dbxresponse;
    }

    public Usr getUsers() {
        return Dbxusers;
    }

    public void setUsers(Usr users) {
        this.Dbxusers = Dbxusers;
    }
}