/*

package com.vishnuDropboxSpringboot.vishnuDropboxSpringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VishnuDropboxSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(VishnuDropboxSpringbootApplication.class, args);
	}
}
*/


package com.vishnuDropboxSpringboot.vishnuDropboxSpringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication
@EnableAutoConfiguration
public class VishnuDropboxSpringbootApplication {
	
	@Bean
	WebMvcConfigurer configurer () {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addResourceHandlers (ResourceHandlerRegistry registry) {
				registry.addResourceHandler("/pages/**").
						addResourceLocations("classpath://Users//user//Desktop//dropbox-springboot//backend//src//main//resources//static//");
			}
		};
	}


	public static void main(String[] args) {
		SpringApplication.run(VishnuDropboxSpringbootApplication.class, args);
	}
}
