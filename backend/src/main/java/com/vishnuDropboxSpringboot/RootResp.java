package com.vishnuDropboxSpringboot;

public class RootResp {

    int rootid;
    Resp response;

    public int getRootid() {
        return rootid;
    }

    public void setRootid(int rootid) {
        this.rootid = rootid;
    }

    public Resp getResponse() {
        return response;
    }

    public void setResponse(Resp response) {
        this.response = response;
    }
}