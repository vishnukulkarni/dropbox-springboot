package com.vishnuDropboxSpringboot;



import java.util.List;

public class ContentResp {

    List<Content> Dbxcontents;
    Resp Dbxresponse;
    int Dbxparentfolderid;

    public List<Content> DbxgetContents() {
        return Dbxcontents;
    }

    public void setDbxContents(List<Content> contents) {
        this.Dbxcontents = Dbxcontents;
    }

    public Resp getDbxResponse() {
        return Dbxresponse;
    }

    public void setDbxResponse(Resp response) {
        this.Dbxresponse = Dbxresponse;
    }

    public int getDbxParentfolderid() {
        return Dbxparentfolderid;
    }

    public void setDbxParentfolderid(int parentfolderid) {
        this.Dbxparentfolderid = Dbxparentfolderid;
    }
}