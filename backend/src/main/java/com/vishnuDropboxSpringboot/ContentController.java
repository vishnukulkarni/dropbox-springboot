package com.vishnuDropboxSpringboot;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;


@RestController    // This means that this class is a Controller
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(path="/") // This means URL's start with /demo (after Application path)
public class ContentController  {

    @Autowired
    private ContentSrc ContentSrc;

    private static String UPLOADED_FOLDER = System.getProperty("user.dir")+ "/src/main/resources/static/";


    @RequestMapping(path="/load",method = RequestMethod.POST)
    public ContentResp getFolderData(@RequestBody Folderdropbox folder) {

        return ContentSrc.getFolderData(folder);
    }

    /*
    @RequestMapping(path="/files",method = RequestMethod.POST)
    public RootResp getRoot(@RequestBody Usr user) {

        return ContentSrc.getRoot(user);
    }

    @RequestMapping(path="/directory",method = RequestMethod.POST)
    public RootResp getdirectory(@RequestBody Usr user) {

        return ContentSrc.getdirectory(user);
    }

     @RequestMapping(path="/star",method = RequestMethod.POST)
    public RootResp getliststart(@RequestBody Usr user) {

        return ContentSrc.getliststart(user);
    }
 @RequestMapping(path="/sharefile",method = RequestMethod.POST)
    public RootResp getsharefile(@RequestBody Usr user) {

        return ContentSrc.getsharefile(user);
    }
*/
    @RequestMapping(path="/createfolder",method = RequestMethod.POST)
    public ContentResp CreateFolder(@RequestBody Folderdropbox folder) {

        return (ContentResp) ContentSrc.CreateFolder(folder);
    }

    @RequestMapping(path="/upload",method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ContentResp fileupload(@RequestParam("file") MultipartFile multipartFile,
                                          @RequestParam("fileparent") String fileparent,
                                          @RequestParam("userid") String userid){

       // String email = (String) session.getAttribute("email");

        Content content = new Content();
        Date date = new Date();
        String virtualname= date+"_"+ multipartFile.getOriginalFilename();
        String filepath = UPLOADED_FOLDER + virtualname;
        //Response response = new Response();
        ContentResp ContentResp = new ContentResp();
        try {



            byte[] bytes = multipartFile.getBytes();
            Path path = Paths.get(filepath);
            Files.write(path, bytes);

            ContentResp = (com.vishnuDropboxSpringboot.ContentResp) ContentSrc.UploadFile(multipartFile.getOriginalFilename(),
                    virtualname,Integer.parseInt(fileparent),Integer.parseInt(userid));


        } catch (IOException e) {
        	
        	/*
            ((Object) response).setStatus("error");
            response.setMsg("upload failed");
            ContentResp.setContents(null);
            ContentResp.setResponse(response);
            
            */
        }


        return ContentResp;

    }



}
