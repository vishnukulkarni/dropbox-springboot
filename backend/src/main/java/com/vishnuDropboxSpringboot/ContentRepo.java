package com.vishnuDropboxSpringboot;



import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContentRepo extends CrudRepository<Content,Integer> {
    List<Content> findAllByUserid(int userid);

    Content findAllByUseridAndOriginalname(int userid, String root);

    List<Content> findAllByContentidIn( List<Integer> contentid);

	void setContents(Object object);

	void setDbxResponse(Resp response);

	void setDbxResponse1(Resp response);

}