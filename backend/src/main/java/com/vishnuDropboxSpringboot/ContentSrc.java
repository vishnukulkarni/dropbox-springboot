package com.vishnuDropboxSpringboot;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.invoke.ConstantCallSite;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContentSrc {

    @Autowired
    private ContentRepo  ContentRepo;
    @Autowired
    private  MapRepo mappingRepo;


    public RootResp getRoot(Usr user){

         RootResp  RootResp = new  RootResp();
        Resp response = new Resp();
        try{

            Content content =   ContentRepo.findAllByUseridAndOriginalname(user.getId(),"root");
             RootResp.setRootid(content.getContentid());
            response.setStatus("success");
            response.setMsg("");
             RootResp.setResponse(response);

        }
        catch (Exception e){
             RootResp.setRootid(0);
            response.setStatus("error");
            response.setMsg("Something went wrong.");
             RootResp.setResponse(response);
        }
        return  RootResp;
    }
    public ContentResp getFolderData(Folderdropbox folder){


        Resp response = new Resp();
        ContentResp ContentResp = new ContentResp();

        try{



            // Get content

            List<Mapping> mapping =  MapRepo.findMappingByFolderidAndUserid(folder.getContentid(),folder.getUserid());

            List<Integer> contentid = mapping.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

             ContentRepo.findAllByContentidIn(contentid);

             //ContentResp.setContents( ContentResp.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("");
            ContentResp.setDbxResponse(response);
            //ContentRepo.setParentfolderid(folder.getContentid());
       }
       catch (Exception e){

           response.setStatus("error");
           response.setMsg("Something went wrong");
           ContentResp.setDbxResponse(response);
           ContentResp.setDbxContents(null);
       }


        return ContentResp;
    }

    public ContentRepo UploadFile(String name, String path, int parentfolderid, int userid ){
        //Resp response = new Response();
        //ContentRepo ContentRepo = new ContentRepo();
        //MapRepo mapping = new Mapping();

        try{

           // Add content start
            Date date = new Date();
            Content content = new Content();
            content.setOriginalname(name);
            content.setVirtualname(path);
            content.setStar("NO");
            content.setDate(date.toString());
            content.setUserid(userid);
            content.setType("file");

            content =  ContentRepo.save(content);

            // End

            // Mapping Start

            //mapping.setContentid(content.getContentid());
            //mapping.setFolderid(parentfolderid);
            //mapping.setUserid(userid);
             //mappingRepo.save(mapping);

            // Mapping End


            List<Mapping> mapping2 =  MapRepo.findMappingByFolderidAndUserid(parentfolderid,userid);

            List<Integer> contentid = mapping2.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

             ContentRepo.findAllByContentidIn(contentid);

            //contentLoadResponse.setContents( ContentRepo.findAllByContentidIn(contentid));
            //response.setStatus("success");
            //response.setMsg("File Successfully uploaded.");
            //ContentRepo.setDbxResponse(response);
            //ContentRepo.setParentfolderid(parentfolderid);

        }
        catch (Exception e){

            //response.setStatus("error");
            //response.setMsg("Error in uploading, Please Try Again.");
            //ContentRepo.setDbxResponse(response);
            //ContentRepo.setDbxContents(null);

        }


        return ContentRepo;
    }

    public ContentRepo CreateFolder(Folderdropbox folder ){

        //Resp response = new Response();
        //ContentRepo ContentRepo = new ContentLoadResponse();
        Mapping mapping = new Mapping();

        try{

            // Add content start
            Date date = new Date();
            Content content = new Content();
            content.setOriginalname(folder.getFoldername());
            content.setVirtualname(folder.getFoldername());
            content.setStar("NO");
            content.setDate(date.toString());
            content.setUserid(folder.getUserid());
            content.setType("folder");

            content =  ContentRepo.save(content);

            // End

            // Mapping Start

            mapping.setContentid(content.getContentid());
            mapping.setFolderid(folder.getContentid());
            mapping.setUserid(folder.getUserid());
             mappingRepo.save(mapping);

            // Mapping End


            List<Mapping> mapping2 =  MapRepo.findMappingByFolderidAndUserid(folder.getContentid(),folder.getUserid());

            List<Integer> contentid = mapping2.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

             ContentRepo.findAllByContentidIn(contentid);
/*
            ContentRepo.setDbxContents( ContentRepo.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("Folder Successfully Created.");
            ContentRepo.setDbxResponse(response);
            ContentRepo.setParentfolderid(folder.getContentid());
            */

        }
        catch (Exception e){
/*
            response.setStatus("error");
            response.setMsg("Error in uploading, Please Try Again.");
            ContentRepo.setDbxResponse(response);
            ContentRepo.setDbxContents(null);
            
            */

        }


        return ContentRepo;
    }

}


/*


public ContentRepo ShareFile(String name, String path, int parentfolderid, int userid ){
        Resp response = new Response();
        ContentRepo ContentRepo = new ContentRepo();
        MapRepo mapping = new Mapping();

        try{

           // Add content start
            Date date = new Date();
            Content content = new Content();
            content.setOriginalname(name);
            content.setVirtualname(path);
            content.setStar("NO");
            content.setDate(date.toString());
            content.setUserid(userid);
            content.setType("file");

            content =  ContentRepo.save(content);

            // End

            // Mapping Start

            mapping.setContentid(content.getContentid());
            mapping.setFolderid(parentfolderid);
            mapping.setUserid(userid);
             mappingRepo.save(mapping);

            // Mapping End


            List<Mapping> mapping2 =  mappingRepo.findMappingByFolderidAndUserid(parentfolderid,userid);

            List<Integer> contentid = mapping2.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

             ContentRepo.findAllByContentidIn(contentid);

            contentLoadResponse.setContents( ContentRepo.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("File Successfully uploaded.");
            ContentRepo.setResponse(response);
            ContentRepo.setParentfolderid(parentfolderid);

        }
        catch (Exception e){

            response.setStatus("error");
            response.setMsg("Error in uploading, Please Try Again.");
            ContentRepo.setResponse(response);
            ContentRepo.setContents(null);

        }


        return ShareFile;
    }




public ContentRepo directory(String name, String path, int parentfolderid, int userid ){
        Resp response = new Response();
        ContentRepo ContentRepo = new ContentRepo();
        MapRepo mapping = new Mapping();

        try{

           // Add content start
            Date date = new Date();
            Content content = new Content();
            content.setOriginalname(name);
            content.setVirtualname(path);
            content.setStar("NO");
            content.setDate(date.toString());
            content.setUserid(userid);
            content.setType("file");

            content =  ContentRepo.save(content);

            // End

            // Mapping Start

            mapping.setContentid(content.getContentid());
            mapping.setFolderid(parentfolderid);
            mapping.setUserid(userid);
             mappingRepo.save(mapping);

            // Mapping End


            List<Mapping> mapping2 =  mappingRepo.findMappingByFolderidAndUserid(parentfolderid,userid);

            List<Integer> contentid = mapping2.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

             ContentRepo.findAllByContentidIn(contentid);

            contentLoadResponse.setContents( ContentRepo.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("File Successfully uploaded.");
            ContentRepo.setResponse(response);
            ContentRepo.setParentfolderid(parentfolderid);

        }
        catch (Exception e){

            response.setStatus("error");
            response.setMsg("Error in uploading, Please Try Again.");
            ContentRepo.setResponse(response);
            ContentRepo.setContents(null);

        }


        return directory;
    }


*/