package com.vishnuDropboxSpringboot;

import org.springframework.data.repository.CrudRepository;

public interface UsrRepo extends CrudRepository<Usr, Integer> {



	Usr findUserByEmail(String email);
	Usr findUsersByEmailAndPassword(String email, String password);
}