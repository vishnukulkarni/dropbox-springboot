package com.vishnuDropboxSpringboot;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Content {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int Dbxcontentid;
    private String Dbxoriginalname;
    private String Dbxvirtualname;
    private String Dbxdate;
    private String Dbxtype;
    private String Dbxstar;
    private int Dbxuserid;

    public Content() {
    }

    public Content(int contentid, String originalname, String virtualname, String date, String type, String star, int userid) {
        this.Dbxcontentid = Dbxcontentid;
        this.Dbxoriginalname = Dbxoriginalname;
        this.Dbxvirtualname = Dbxvirtualname;
        this.Dbxdate = Dbxdate;
        this.Dbxtype = Dbxtype;
        this.Dbxstar = Dbxstar;
        this.Dbxuserid = Dbxuserid;
    }

    public int getContentid() {
        return Dbxcontentid;
    }

    public void setContentid(int contentid) {
        this.Dbxcontentid = Dbxcontentid;
    }

    public String getOriginalname() {
        return Dbxoriginalname;
    }

    public void setOriginalname(String Dbxoriginalname) {
        this.Dbxoriginalname = Dbxoriginalname;
    }

    public String getVirtualname() {
        return Dbxvirtualname;
    }

    public void setVirtualname(String Dbxvirtualname) {
        this.Dbxvirtualname = Dbxvirtualname;
    }

    public String getDate() {
        return Dbxdate;
    }

    public void setDate(String Dbxdate) {
        this.Dbxdate = Dbxdate;
    }

    public String getType() {
        return Dbxtype;
    }

    public void setType(String Dbxtype) {
        this.Dbxtype = Dbxtype;
    }

    public String getStar() {
        return Dbxstar;
    }

    public void setStar(String Dbxstar) {
        this.Dbxstar = Dbxstar;
    }

    public int getUserid() {
        return Dbxuserid;
    }

    public void setUserid(int Dbxuserid) {
        this.Dbxuserid = Dbxuserid;
    }
}
