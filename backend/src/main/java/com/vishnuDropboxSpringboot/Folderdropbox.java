
package com.vishnuDropboxSpringboot;

public class Folderdropbox {
    int dbxuserid;
    int dbxcontentid;
    String  dbxfoldername;

    public String getFoldername() {
        return dbxfoldername;
    }

    public void setFoldername(String foldername) {
        this.dbxfoldername = dbxfoldername;
    }

    public int getUserid() {
        return dbxuserid;
    }

    public void setUserid(int userid) {
        this.dbxuserid = dbxuserid;
    }

    public int getContentid() {
        return dbxcontentid;
    }

    public void setContentid(int contentid) {
        this.dbxcontentid = dbxcontentid;
    }
}