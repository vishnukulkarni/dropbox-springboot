package com.vishnuDropboxSpringboot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Usr {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer Dbxid;

    private String Dbxfirstname;

    private String Dbxlastname;

    private String Dbxemail;

    private String Dbxpassword;

    private String Dbxaboutme;

    private String Dbxinterests;

    private String Dbxsuggestions;



    public Integer getId() {
        return Dbxid;
    }

    public void setId(Integer id) {
        this.Dbxid = id;
    }

    public String getFirstname() {
        return Dbxfirstname;
    }

    public void setFirstname(String firstname) {
        this.Dbxfirstname = firstname;
    }

    public String getLastname() {
        return Dbxfirstname;
    }

    public void setLastname(String lastname) {
        this.Dbxlastname = lastname;
    }

    public String getEmail() {
        return Dbxemail;
    }

    public void setEmail(String email) {
        this.Dbxemail = email;
    }

    public String getPassword() {
        return Dbxpassword;
    }

    public void setPassword(String password) {
        this.Dbxpassword = password;
    }

    public String getAboutme() {
        return Dbxaboutme;
    }

    public void setAboutme(String aboutme) {
        this.Dbxaboutme = aboutme;
    }

    public String getInterests() {
        return Dbxinterests;
    }

    public void setInterests(String interests) {
        this.Dbxinterests = interests;
    }

    public String getAll() {
        return Dbxsuggestions;
    }

    public void setAll(String suggestions) {
        this.Dbxsuggestions = suggestions;
    }


}